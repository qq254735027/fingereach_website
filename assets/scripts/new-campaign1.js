var NewCampaign1 = function () {

	var EnumValidate = {
		Empty: 0,
        CampaignName: 1,
        StartDate: 2,
		EndDate: 3,
		TimeSlot: 4,
		Country: 5,
		City:6,
		Language:7,
		Distrubution: 8,
		DeliveryMode: 9,
		Device: 10,
		OS: 11
	}
	
	//boolean array used for validation
	var array_flag=new Array([12]);	
	for(var i=0; i<=11; i++){
		array_flag[i]=false;
	}
	
	//string array used for storing error message
	var array_message=new Array([12]);
	for(var i=0; i<=11; i++){
		array_message[i]="";
	}
	
	//country style effect
	function format(state) {
		if (!state.id) return state.text; // optgroup
		return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
	}
	var formInit1 = function() {		
		$("#id_country").select2({
			allowClear: true,
			formatResult: format,
			formatSelection: format,
			escapeMarkup: function (m) {
				return m;
			}
		});		
	}

	var formInit2 = function() {		
		//default selected, must be first initialized before being shown
		$("#multi_select_newcampaign_inapp").val(["games", "social_networking"]);		
		$('#multi_select_newcampaign_inapp').multiSelect({
			selectableOptgroup: true
		});       

		//initialize xeditable textarea
		$('#placements_new_campaign').editable({
			value: 'aaa\nasdf',
			showbuttons: 'bottom'
		});   	
	}

	var validate = function() {
		//campaign name
		if(""==$('#campaign_name').val()){
			$('#label_warning_campaign_name').text("Campaign name cannot be null.");
			$('#label_warning_campaign_name').show();
		}
		
		//start date - end date
		var start_date=$("#start_date").val().trim();
		var end_date=$("#end_date").val();				
		if(splitDate(start_date)>=splitDate(end_date)){
			$('#label_warning_end_date').text("End date should not be earlier than start date.");
			$('#label_warning_end_date').show();
		}else{
			$('#label_warning_end_date').hide();
		}		
		
		//time slot 
		var length_table_time_slot=$("#table_add_time_slot tr").length-1;
		if(0==length_table_time_slot && "specify"==$("input[name='time_slot']:checked").val()){
			$('#label_warning_time_slot').text("You should specify at least one time slot.");
			$('#label_warning_time_slot').show();
		}else{
			$('#label_warning_time_slot').hide();
		}
		
		//location
		var length_table_location=$("#table_location tr").length-1;
		if(0==length_table_location && $('#id_country').val()!=""){
			$('#label_warning_location').text("You should specify at least one location.");
			$('#label_warning_location').show();
		}else{
			$('#label_warning_location').hide();
		}
		
		//language
		var length_checkbox_language=$("input[name='checkbox_language']:checked").length;
		if(0==length_checkbox_language && "customize"==$("input[name='language']:checked").val()){
			$('#label_warning_language').text("Please select at least one language.");
			$("#label_warning_language").show();
		}else{
			$("#label_warning_language").hide();
		}
		
		//delivery mode
		if("budget-based"==$("input[name='delivery_mode']:checked").val()){
			if(""==$("#total_budget1").val()){
				$('#p_warning_budget_based').text("Please input total budget.");
				$("div[name='div_warning_budget_based']").show();
			}else{
				$("div[name='div_warning_budget_based']").hide();
			}
		}else{		
			if(""==$("#target_impressions").val()){
				$('#p_warning_impression_based').text("Please input target impressions.");
				$("div[name='div_warning_impression_based']").show();
			}else if(""==$("#max_budget1").val()){
				$('#p_warning_impression_based').text("Please input max budget.");
				$("div[name='div_warning_impression_based']").show();
			}else{
				$("div[name='div_warning_impression_based']").hide();
			}
		}
		
		//device
		if("customize"==$("input[name='device']:checked").val() && "Empty"==$("#xeditable-device").html()){
			$('#p_warning_device').text("Please select device make.");
			$("div[name='div_warning_device']").show();
		}else{
			$("div[name='div_warning_device']").hide();
		}
		
		//os
		if("customize"==$("input[name='os']:checked").val() && "Empty"==$("#xeditable-os").html()){
			$('#p_warning_os').text("Please select device os.");
			$("div[name='div_warning_os']").show();
		}else{
			$("div[name='div_warning_os']").hide();
		}
	}
	
	//return Date
	var splitDate=function(date){
		var array_split=date.replace(/-/g, " ").replace(/\s+/g, "/").split("/");
		if("January"==array_split[1]){
			array_split[1]=1;
		}else if("February"==array_split[1]){
			array_split[1]=2;
		}else if("March"==array_split[1]){
			array_split[1]=3;
		}else if("April"==array_split[1]){
			array_split[1]=4;
		}else if("May"==array_split[1]){
			array_split[1]=5;
		}else if("June"==array_split[1]){
			array_split[1]=6;
		}else if("July"==array_split[1]){
			array_split[1]=7;
		}else if("August"==array_split[1]){
			array_split[1]=8;
		}else if("September"==array_split[1]){
			array_split[1]=9;
		}else if("October"==array_split[1]){
			array_split[1]=10;
		}else if("November"==array_split[1]){
			array_split[1]=11;
		}else if("December"==array_split[1]){
			array_split[1]=12;
		}
		return new Date(array_split[2]+"/"+array_split[1]+"/"+array_split[0]+" "+array_split[3]);
	}

	//dynamically insert a line into table time slot
	var addTimeSlotSpecify=function(from, to, weekday){
		var _len = $("#table_add_time_slot tr").length;
		$("#table_add_time_slot").append(
				"<tr id='id_table_time_slot"+_len+"'>"
				+"<td>"+_len+"</td>"
				+"<td>"+weekday+"</td>"
				+"<td>"+from+"</td>"
				+"<td>"+to+"</td>"
				+"<td><a id='name_time_slot_delete" + _len + "' value="+_len+" class=\"btn mini black\"><i class=\"icon-trash\"></i> Delete</a></td>"
				+"</tr>"
		);
		//add click listener, delete one row in time slot specify
		$("a[id='name_time_slot_delete"+_len+"']").one("click", function() {							
			deleteTimeSlotSpecify(_len);									
		});
	}

	//dynamically delete a line of table time slot
	var deleteTimeSlotSpecify =function(index)	//index begins from 1
	{
		//alert(index);
		if(index<=0){
			return;
		}
		var _len = $("#table_add_time_slot tr").length;
			
		//$("a[id='name_table_time_slot"+index+"']").unbind("click");
		$("tr[id='id_table_time_slot"+index+"']").remove();
		for(var i=index,j=_len;i<j;i++)
		{           
			var $td = $("tr[id='id_table_time_slot"+i+"']").children('td');	//get all td content of current tr
			//alert($td.eq(1).text());
			
			$("tr[id='id_table_time_slot"+i+"']")
			.replaceWith("<tr id='id_table_time_slot"+(i-1)+"'>"
					+"<td>"+(i-1)+"</td>"
					+"<td>"+$td.eq(1).text()+"</td>"
					+"<td>"+$td.eq(2).text()+"</td>"
					+"<td>"+$td.eq(3).text()+"</td>"
					+"<td><a id='name_time_slot_delete" + (i-1) + "' value="+(i-1)+" class=\"btn mini black\"><i class=\"icon-trash\"></i> Delete</a></td>"
					+"</tr>");
			//unbind click
			$("a[id='name_time_slot_delete"+(i-1)+"']").unbind("click");
			//add click listener, delete one row in time slot specify
			$("a[id='name_time_slot_delete"+(i-1)+"']").one("click", function() {							
				deleteTimeSlotSpecify($(this).attr('value'));
			});
		}    		
	}
	
	var addLocation=function(country, city){
		var _len = $("#table_location tr").length;
		$("#table_location").append(
				"<tr id='id_table_location"+_len+"'>"
				+"<td>"+_len+"</td>"
				+"<td>"+country+"</td>"
				+"<td>"+city+"</td>"
				+"<td><a id='id_table_location_delete" + _len + "' value="+_len+" class=\"btn mini black\"><i class=\"icon-trash\"></i> Delete</a></td>"
				+"</tr>"
		);
		//add click listener, delete one row in time slot specify
		$("a[id='id_table_location_delete"+_len+"']").click(function() {							
			deleteLocation(_len);									
		});
	}
	
	var deleteLocation =function(index){
		
		if(index<=0){
			return;
		}
		
		var _len = $("#table_location tr").length;
		$("a[id='id_table_location_delete"+index+"']").unbind("click");
		$("tr[id='id_table_location"+index+"']").remove();
		//alert("_len:"+_len+" index:"+index);
		for(var i=index; i<=_len-1; i++)
		{          
			//alert("i:"+i);
			var $td = $("tr[id='id_table_location"+i+"']").children('td');	//get all td content of current tr
			//alert($td.eq(1).text());
			
			$("tr[id='id_table_location"+i+"']")
			.replaceWith("<tr id='id_table_location"+(i-1)+"'>"
					+"<td>"+(i-1)+"</td>"
					+"<td>"+$td.eq(1).text()+"</td>"
					+"<td>"+$td.eq(2).text()+"</td>"
					+"<td><a id='id_table_location_delete" + (i-1) + "' value="+(i-1)+" class=\"btn mini black\"><i class=\"icon-trash\"></i> Delete</a></td>"
					+"</tr>");
			//unbind click
			$("a[id='id_table_location_delete"+(i-1)+"']").unbind("click");
			//add click listener, delete one row in time slot specify
			$("a[id='id_table_location_delete"+(i-1)+"']").click(function() {							
				deleteLocation($(this).attr('value'));
			});
		}    		

	}
	
	// if d1>=d2 return true  
	function compareDate(d1, d2) {   
		return Date.parse(d1.replace(/-/g, "/")) >= Date.parse(d2.replace(/-/g, "/"))   
	}

	return {
		//main function to initiate the module
		init: function () {			
			formInit1();
			formInit2();

			$("#start_date").val(Date.today().toString('dd MMMM yyyy - hh:mm'));	//today date
			//addTimeSlotSpecify("10:00 AM", "12:00 PM", "Monday");
			//addTimeSlotSpecify();
			//addTimeSlotSpecify();

			$('#btn_next1').click(function() {
				validate();
				App.scrollTo();
			});

			$('#aa').click(function() {
				$(':input','#form_new_campaign1')
				.not(':button, :submit, :reset, :hidden, input[name=\"start_date\"]')	//clear all but 
				.val('')
				.removeAttr('checked')
				.removeAttr('selected');
				formInit1();
			});
			
			//add location
			$('#btn_add_location').click(function() {
				var country=$("select[name='country']").find("option:selected").text();
				console.log(country);
				var city=$('#id_city').val();
				if(""==city && country!=""){
					$('#label_warning_location').text('Please input specific city!');
					$('#label_warning_location').show();
				}else if(""==country){
					return;
				}else{
					$('#label_warning_location').hide();
					addLocation(country, city);
				}
			});

			//get multi_select_newcampaign_inapp selected value
			$('#b2').click(function() {
				var array=$('#multi_select_newcampaign_inapp').val();
				var s="";
				//for each method
				$.each(array, function(i, n){
					s=s+n+" ";
				});
				//directly access
				//alert(array[0]);
				//get length
				//alert(array.length);
			});

			
			//read time slot specify param
			$('#btn_add_time_slot_specify').click(function() {
				var from=$('#from_add_time_slot_specify').val();
				var to=$('#to_add_time_slot_specify').val();
				var weekday=$('#weekday_add_time_slot_specify').val();

				//alert(Date.parse(from.replace(/-/g, "/")));

				if($('#weekday_add_time_slot_specify').val()==''){
					$('#label_warning_time_slot_modal').text('Please select a week day!');
					$('#label_warning_time_slot_modal').show();
				}else if(true==compareDate(from, to)){
					$('#label_warning_time_slot_modal').text('Time span error!');
					$('#label_warning_time_slot_modal').show();
				}
				else{
					addTimeSlotSpecify(from, to, weekday);				
					$(this).attr('data-dismiss', 'modal');	
					$('#label_warning_time_slot_modal').hide();	
				}						
			});
			
			//open time slot specify modal
			$('#btn_open_time_slot').click(function() {
				$('#form_modal5').dailog();
			});
			
			//time slot radio change
			$("input[name='time_slot']").change(function() {
				if ($("input[name='time_slot']:checked").val() == 'all')
					$("#time_slot_table").hide();
				else
					$("#time_slot_table").show();
			});
			
			//language radio change
			$("input[name='language']").change(function() {
				if ($("input[name='language']:checked").val() == 'all'){
					$("div[name='div_language']").hide();
					$("div[name='div_warning_language']").hide();
				}
				else{
					$("div[name='div_language']").show();
					$("#label_warning_language").hide();
				}
			});
			
			//delivery mode radio change
			$("input[name='delivery_mode']").change(function() {
				if ($("input[name='delivery_mode']:checked").val() == 'budget-based'){
					$("div[name='name_budget_based']").show();
					$("div[name='name_impression_based']").hide();
					$("div[name='div_warning_impression_based']").hide();
				}
				else{
					$("div[name='name_budget_based']").hide();
					$("div[name='name_impression_based']").show();
					$("div[name='div_warning_budget_based']").hide();
				}
			});
			
			//device radio change
			$("input[name='device']").change(function() {
				if ($("input[name='device']:checked").val() == 'all'){
					$("#device_customize").hide();
				}
				else{
					$("#device_customize").show();
					$("div[name='div_warning_device']").hide();
				}
			});
			
			//os radio change
			$("input[name='os']").change(function() {
				if ($("input[name='os']:checked").val() == 'all')
					$("#os_customize").hide();
				else{
					$("#os_customize").show();
					$("div[name='div_warning_os']").hide();
				}
			});

		}

	};

}();