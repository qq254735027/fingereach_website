var TableAllAdGroups = function () {

     var initTable = function() {
        var oTable = $('#table_all_ad_groups').dataTable( {           
            "aoColumnDefs": [
                { "aTargets": [ 0 ] }
            ],
            "aaSorting": [[1, 'asc']],
             "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 5,
        });

        jQuery('#table_all_ad_groups_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
        jQuery('#table_all_ad_groups_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
        jQuery('#table_all_ad_groups_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#table_all_ad_groups_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            initTable();

        }

    };

}();